"use strict";

/* Métodos de Abertura */

Object.defineProperties( Node.prototype,
  { oChangeBySize:
      { value: function oChangeBySize( media, truthFunction, falsyFunction, evaluedNode )
          { var targetComponent;
            let acceptedMediaKeys = [ [ "innerWidth", "innerHeight", "outerWidth", "outerHeight" ],
                                      [ "width", "height", "availWidth", "availHeight" ],
                                      [ "clientWidth", "clientHeight", "offsetWidth", "offsetHeight", "scrollWidth", "scrollHeight" ] ];
            for( let size in media )
              { if( typeof media[ size ] !== "number" ) throw new TypeError( "Invalid object value in media argument of oChangeBySize function." );
                let sizeID;
                for( let i = 0; i < acceptedMediaKeys.length; i++ )
                  { if( acceptedMediaKeys[i].includes( size ) ) { sizeID = i; break } }
                switch( sizeID )
                  { case 0: targetComponent = window; break;
                    case 1: targetComponent = window.screen; break;
                    case 2: targetComponent = evaluedNode || this; break;
                    default: throw new TypeError( "Invalid object key in media argument of oChangeBySize function." ) }
                if( targetComponent[ size ] > media[ size ] ) return falsyFunction ? falsyFunction() : false }
            return truthFunction ? truthFunction() : true } },
    oChangePlacementBySize: // Realiza a inserção do alvo em um receptáculo em função do tamanho de dado objeto
      { value: function oChangePlacementBySize( media, receiver, nextSibling, evaluedNode )
          { let originalParent = this.originalParent = this.originalParent || this.parentElement,
                originalSibling = this.originalSibling = this.originalSibling || this.nextElementSibling || "none";
            if( this.oChangeBySize( media, false, false, evaluedNode || receiver ) )
              { if( !Array.from( receiver.children ).includes( this ) ) nextSibling ? receiver.insertBefore( this, nextSibling ) : receiver.appendChild( this ) }
            else
              { if( Array.from( receiver.children ).includes( this ) ) originalSibling != "none" ? originalParent.insertBefore( this, originalSibling ) : originalParent.appendChild( this ) } } } } );

Object.defineProperties( HTMLElement.prototype,
  { oFlatSize: // Equaliza Medidas do Alvo às do Agente
      { value: function oFlatSize( element = this.firstElementChild, targetSizes = { height: "offsetHeight" }, multiplier = 1 )
          { for( let size in targetSizes )
              { if( ![ "width", "height" ].includes( size ) ) throw new RangeError( "Object key in targetSizes argument of oFlatSize function must be 'width' or 'height'." )
                let allowedSizes = [ "clientWidth", "clientHeight", "offsetWidth", "offsetHeight", "scrollWidth", "scrollHeight" ];
                if( !allowedSizes.includes( targetSizes[ size ] ) ) throw new RangeError( "Object value in targetSizes argument of oFlatSize function must be an element size property." );
                this.style[ size ] = element[ targetSizes[ size ] ] * multiplier + "px" } } },
    oEncompassChildren: // Expande Medidas do Alvo até que Alcance a Total de seus Descendentes
      { value: function oEncompassChildren( width = true, height = true )
          { if( width ) this.style.width = this.scrollWidth + "px";
            if( height ) this.style.height = this.scrollHeight + "px" } } } );

/* Identificadores Globais */

const topHeader = document.getElementById( "top-header" ); // Cabeçalho Superior
topHeader:
  { let header = topHeader;
    header.container = header.querySelector( ".content-container" );
    header.menu = header.querySelector( ".nav-container" ); /* Menu do Cabeçalho Superior */
    contactMenu:
      { let menu = header.menu.contact = header.querySelector( ".contact-container" ) /* Menu com Informações de Contato */ }
    linksMenu:
      { let menu = header.menu.links = header.querySelector( ".links-container" ); /* Menu com Links Diversos */
        menu.pages = menu.querySelector( ".pages-list" ); /* Lista de Páginas Internas do Site */
        menu.social = menu.querySelector( ".social-list" ) /* Lista de Links para Perfis da Empresa nas Redes Sociais */ }
    header.logo = header.querySelector( ".logo" ); /* Logomarca da Empresa */
    header.buttons = header.querySelector( ".buttons-container" ) /* Conjunto de Botões de Navegação pelo Cabeçalho Superior */ }

/* Ouvintes de Eventos */

changeTopHeaderMenuDisplay: // Alterar Exibição do Menu do Cabeçalho Superior em Função do Tamanho de Tela
  { let contactMenu = topHeader.menu.contact,
        pagesList = topHeader.menu.links.pages, socialList = topHeader.menu.links.social,
        targetWidth = 1049;
    changePagesListDisplay: // Altera a Exibição da Lista de Páginas com Links do Site para uma Deslizante
      { let events = [ "load", "resize" ];
        for( let _event of events )
          { window.addEventListener( _event, () => pagesList.oChangeBySize( { innerWidth: targetWidth },
              () => { pagesList.classList.add( "js-sliding" ); pagesList.oFlatSize() },
              () => { pagesList.classList.remove( "js-sliding" ); pagesList.oEncompassChildren( false ) } ) ) } }
    changeSocialListPlacement: // Altera a Posição da Lista com Links de Redes Sociais para que Apareça no Menu de Contatos
      { window.addEventListener( "resize", () => socialList.oChangePlacementBySize( { innerWidth: targetWidth }, contactMenu ) ) } }

changeTopHeaderLogoDisplay: // Alterar Posicionamento da Logomarca em Função do Tamanho de Tela
  { let container = topHeader.container, logo = topHeader.logo, buttons = topHeader.buttons;
    window.addEventListener( "resize", () => logo.oChangePlacementBySize( { innerWidth: 699 }, container, buttons ) ) }

slideTopHeaderMenu: // Deslizar Menu Responsivo do Cabeçalho Superior
  { let pagesList = topHeader.menu.links.pages;
    pagesList.addEventListener( "click", () =>
      { if( !pagesList.classList.contains( "js-sliding" ) ) return;
        pagesList.classList.toggle( "slided" );
        pagesList.classList.contains( "slided" ) ? pagesList.oEncompassChildren( false ) : pagesList.oFlatSize() } ) }

/* Instruções Iniciais */

changeTopHeaderMenuDisplay: // Alterar Exibição do Menu do Cabeçalho Superior em Função do Tamanho de Tela
  { let contactMenu = topHeader.menu.contact,
        pagesList = topHeader.menu.links.pages, socialList = topHeader.menu.links.social,
        targetWidth = 1049;
    socialList.oChangePlacementBySize( { innerWidth: targetWidth }, contactMenu ) }

changeTopHeaderLogoDisplay: // Alterar Posicionamento da Logomarca em Função do Tamanho de Tela
  { let container = topHeader.container, logo = topHeader.logo, buttons = topHeader.buttons;
    logo.oChangePlacementBySize( { innerWidth: 699 }, container, buttons ) }
