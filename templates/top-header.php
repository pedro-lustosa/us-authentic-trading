<?php # Main header of the site
echo __DIR__;
?>
<header id="top-header">
  <nav class="nav-container">
    <address class="contact-info">
      <p class="message"><strong class="action">Call Now</strong> <span class="tel">&lpar;310&rpar; 961 6276</span></p>
    </address>
    <picture class="logo">
      <img src="multimedia/images/icons/logo-1.png" alt="logo">
    </picture>
    <div class="links-container">
      <ul class="pages-list">
        <li class="partnerships current"><a href="">Partnerships &amp; Vision</a></li>
        <li class="team"><a href="">Team</a></li>
        <li class="help"><a href="">Get in Touch</a></li>
      </ul>
      <ul class="social-list">
        <li class="facebook"><a href="#"></a></li>
        <li class="twitter"><a href="#"></a></li>
        <li class="linkedin"><a href="#"></a></li>
        <li class="youtube"><a href="#"></a></li>
      </ul>
    </div>
  </nav>
  <div class="heading-container">
    <div class="text-container">
      <h1 class="title">
        <span class="leading-heading"><span class="thin">Prevent</span> Counterfeiting</span>
        <span class="display-heading">in China</span>
      </h1>
      <p class="text">
        We work directly with the Chinese government to prevent, deter, and eliminate counterfeits of your valuable products and brands.
      </p>
    </div>
    <picture class="side-image">
      <img src="multimedia/images/decoratives/goods-1.png" alt="">
    </picture>
  </div>
  <div class="buttons-container">
    <div class="buttons-set">
      <button type="button" name="previous">
        <img src="multimedia/images/icons/rarr-1.png" alt="Previous">
      </button>
      <button type="button" name="next">
        <img src="multimedia/images/icons/rarr-1.png" alt="Next">
      </button>
    </div>
  </div>
</header>
