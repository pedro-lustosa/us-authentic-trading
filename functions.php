<?php

# Constantes

define( 'BASE_DIR', get_template_directory_uri() . DIRECTORY_SEPARATOR );

# Funções

function set_stylesheets()
  { wp_register_style( 'home', BASE_DIR . '/styles/home/home-min.css' );
    wp_enqueue_style( 'home' ); };

add_action( 'wp_enqueue_scripts', 'set_stylesheets' );

# Instruções Iniciais
  // Menus do Tema
  register_nav_menus(
    [ 'top_header' => __( "Main Header Menu" ), 'top_footer' => __( "Main Footer Menu" ) ] );

?>
