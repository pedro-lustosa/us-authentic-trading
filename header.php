<!DOCTYPE html>
<html <?php language_attributes()?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ) ?>">
    <meta name="creator" content="This theme was created by the brazilian company DR Estúdio &ndash; https://danielrothier.com">
    <meta name="description" content="<?php bloginfo( 'description' ) ?>">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">

    <base href="<?= BASE_DIR ?>" target="_self">
    <title><?php bloginfo( 'name' ) ?></title>

    <link rel="icon" href="multimedia/images/icons/logo-1.png">
    <?php wp_head() ?>
  </head>
  <body>
