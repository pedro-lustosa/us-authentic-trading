<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="creator" content="This theme was created by the brazilian company DR Estúdio &ndash; https://danielrothier.com">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">

    <title>US Authentic Trading</title>

    <link rel="icon" href="multimedia/images/icons/logo-1.png">

    <link rel="stylesheet" href="styles/home/home-min.css">
    <script defer src="scripts/home/home.js" ></script>
  </head>
  <body>
    <header id="top-header">
      <div class="content-container">
        <nav class="nav-container">
          <div class="contact-container">
            <address class="contact-info">
              <p class="message"><strong class="action">Call Now</strong> <span class="tel">&lpar;310&rpar; 961 6276</span></p>
            </address>
          </div>
          <picture class="logo">
            <img src="multimedia/images/icons/logo-1.png" alt="logo">
          </picture>
          <div class="links-container">
            <ul class="pages-list">
              <!-- <li class="partnerships"><a href="">Partnerships<span class="removable"> &amp; Vision</span></a></li>
              <li class="team"><a href="">Team</a></li>
              <li class="help"><a href="">Get in Touch</a></li> -->
            </ul>
            <ul class="social-list">
              <!-- <li class="facebook"><a href="#"></a></li>
              <li class="twitter"><a href="#"></a></li>
              <li class="linkedin"><a href="#"></a></li>
              <li class="youtube"><a href="#"></a></li> -->
            </ul>
          </div>
        </nav>
        <div class="heading-container">
          <div class="text-container">
            <h1 class="title">
              <span class="leading-heading"><span class="thin">Prevent</span> Counterfeiting</span>
              <span class="display-heading">in China</span>
            </h1>
            <p class="text">
              We work directly with the Chinese government to prevent, deter, and eliminate counterfeits of your valuable products and brands.
            </p>
          </div>
          <picture class="side-image">
            <img src="multimedia/images/decoratives/goods-1.png" alt="">
          </picture>
        </div>
        <div class="buttons-container">
          <div class="buttons-set">
            <button type="button" name="previous">
              <img src="multimedia/images/icons/rarr-1.png" alt="Previous">
            </button>
            <button type="button" name="next">
              <img src="multimedia/images/icons/rarr-1.png" alt="Next">
            </button>
          </div>
        </div>
      </div>
    </header>
    <main id="top-content">
      <article id="about-us">
        <section id="introduction">
          <div class="content-container">
            <picture class="side-image">
              <img src="multimedia/images/decoratives/city-2.jpg" alt="">
            </picture>
            <div class="text-container">
              <header class="heading-container">
                <hgroup class="heading-set">
                  <h3 class="sub-title">
                    About Us
                  </h3>
                  <h2 class="title">
                    <span class="line">US Authentic</span>
                    <span class="line">Trading Company</span>
                  </h2>
                </hgroup>
              </header>
              <div class="texts-set">
                <p class="text">
                  US Authentic is the exclusive US brand-sourcing and development partner of Century Stage / Central Leader and the Waterdrop e-commerce platform. In partnership with the Chinese government and major commercial stakeholders, Waterdrop is deploying an innovative factory-to-consumer e-commerce platform and disruptive sales model to establish a trusted purchasing and distribution platform for the import and sale of global brands within mainland China. US Authentic is currently on-boarding and preparing select US brands to sell their products on the Waterdrop platform.
                </p>
              </div>
            </div>
          </div>
        </section>
        <section id="what-we-do">
          <section id="solutions">
            <div class="content-container">
              <div class="text-container">
                <header class="heading-container">
                  <hgroup class="heading-set">
                    <h4 class="sub-title">What we Do</h4>
                    <h3 class="title">Our Solution</h3>
                  </hgroup>
                </header>
                <div class="texts-set">
                  <p class="text">
                    USAT maintains an extensive network throughout China, spanning both corporate and top level government relationships. We work with thought leaders and power brokers at the forefront of China’s economic transformation to solve the dual problems of counterfeiting and market penetration, massive pain points that undermine the accelerating demand for high quality consumer brands, healthcare products and services, medical technology, and pharmaceuticals within the ascendant Chinese middle and upper classes.
                  </p>
                  <p class="text">
                    Our solution represents a paradigm shift in security, social marketing, and culturally informed brand creation as well as top down healthcare marketplace access. We go beyond protecting our brands to build a self-reinforcing cycle of growth, available as a premium turnkey solution. You don’t need to trust China: you can trust us.
                  </p>
                </div>
                <div class="images-set">
                  <picture>
                    <img src="multimedia/images/icons/seal-1.png" alt="US Authentic seal">
                  </picture>
                  <picture>
                    <img src="multimedia/images/icons/seal-2.png" alt="Wujei seal">
                  </picture>
                </div>
              </div>
              <div class="side-images">
                <figure class="image-container">
                  <picture class="ilustration">
                    <img src="multimedia/images/decoratives/ilustration-1.png" alt="">
                  </picture>
                  <figcaption class="description-container">
                    <h1 class="title">Secure Warehousing, Customs, and Distribution</h1>
                    <p class="description">End-to-end control, from factory to consumer.</p>
                  </figcaption>
                  <div class="button-container">
                    <button type="button" name="know-more"></button>
                  </div>
                </figure>
                <figure class="image-container">
                  <picture class="ilustration">
                    <img src="multimedia/images/decoratives/ilustration-2.png" alt="">
                  </picture>
                  <figcaption class="description-container">
                    <h1 class="title">E-commerce 4.0 Sales and Marketing Platform</h1>
                    <p class="description">A new way to market, the best way to sell, the only way to connect.</p>
                  </figcaption>
                  <div class="button-container">
                    <button type="button" name="know-more"></button>
                  </div>
                </figure>
                <figure class="image-container">
                  <picture class="ilustration">
                    <img src="multimedia/images/decoratives/ilustration-3.png" alt="">
                  </picture>
                  <figcaption class="description-container">
                    <h1 class="title">KOL-Driven Demand Generation and Education</h1>
                    <p class="description">The most sought after KOLs, the best brands, the most passionate consumers.</p>
                  </figcaption>
                  <div class="button-container">
                    <button type="button" name="know-more"></button>
                  </div>
                </figure>
              </div>
            </div>
          </section>
          <aside id="brand-action">
            <div class="content-container">
              <div class="text-container">
                <header class="heading-container">
                  <h3 class="title">
                    <span class="line">Are You a Premier</span>
                    <span class="line">USA Brand?</span>
                  </h3>
                </header>
                <div class="text-set">
                  <p class="text">
                    Make a complete assessment of your brand or product for the Chinese consumer market using the most advanced mobile social e-commerce platform and the most powerful and protective infrastructure for product sales in China.
                  </p>
                </div>
              </div>
              <div class="button-container">
                <button type="button" name="contact">Contact us now</button>
              </div>
            </div>
          </aside>
          <section id="healthcare">
            <div class="content-container">
              <div class="texts-block">
                <picture class="side-image">
                  <img src="multimedia/images/decoratives/health-1.jpg" alt="">
                </picture>
                <div class="text-container side">
                  <header class="heading-container">
                    <hgroup class="heading-set">
                      <h4 class="sub-title">What we Do</h4>
                      <h3 class="title">Healthcare</h3>
                    </hgroup>
                  </header>
                  <div class="text-set">
                    <p class="text">
                      USAT possesses unprecedented access to the Chinese healthcare market via direct submissions to national hospital procurement tenders, bonded duty free warehouses, exhibition centers for pre-FDA and/or pre-CFDA device sales, China FDA accelerated approval channels, clinical trial implementation and patient access, educational services for Chinese physicians, capital investment from PRC to name a few. USAT is able to implement market access strategies, spanning a broad range of medical research networks, medical treatment and device technologies, biological and molecular pharmaceuticals, institutional and education services and inroads for the emergent field of global e-medicine and regenerative medicine including gene therapy and cellular therapy.
                    </p>
                  </div>
                </div>
                <div class="text-container full">
                  <div class="text-set">
                    <p class="text">
                      Dr. Michael Edwards M.D. PhD—co-founder and CEO of USAT—specializes in the fields of genetics and regenerative stem cell therapy. He’s spent the past decade developing an extensive network of researchers and officials within the Chinese healthcare ecosystem, and is a world leader in facilitating the flow of this specialized knowledge between East and West. Most notably, this has taken the form of a partnership between the Chinese government and the USAT team to create an advanced centers for the development and training of next-generation e-medicine practices and technologies at the new Hawaii Bioskills facilities. These relationships will be further cemented at the Hawaii Health Span Summit which will held in May 2019 bringing in the world leaders in Regenerative Medicine and Public Health from China and the West to bridge relationships between participating countries scientists, technologists, physicians, institutions, educators, and corporations.
                    </p>
                  </div>
                </div>
              </div>
              <div class="images-block">
                <figure class="image-container">
                  <picture class="ilustration">
                    <img src="multimedia/images/decoratives/ilustration-4.png" alt="">
                  </picture>
                  <figcaption class="description-container">
                    <h1 class="title">Medical Devices</h1>
                    <p class="description">
                      We possess relationships with an extensive (national level) procurement network, with demand for devices ranging from medical IoT to imaging hardware. We also have the ability to accelerate CFDA approval for our medical device clients.
                    </p>
                  </figcaption>
                </figure>
                <figure class="image-container">
                  <picture class="ilustration">
                    <img src="multimedia/images/decoratives/ilustration-5.png" alt="">
                  </picture>
                  <figcaption class="description-container">
                    <h1 class="title">Pharmaceuticals</h1>
                    <p class="description">
                      By way of exclusive partnerships, we have the capacity to supply and distribute Western pharmaceuticals to the China market at the national level. We also have the ability to import pre-CFDA pharmaceuticals.
                    </p>
                  </figcaption>
                </figure>
                <figure class="image-container">
                  <picture class="ilustration">
                    <img src="multimedia/images/decoratives/ilustration-6.png" alt="">
                  </picture>
                  <figcaption class="description-container">
                    <h1 class="title">Healthcare Services</h1>
                    <p class="description">
                      We are currently developing multiple Western standard collaborative healthcare facilities throughout China, combining cutting edge technology with luxury amenities.
                    </p>
                  </figcaption>
                </figure>
                <figure class="image-container">
                  <picture class="ilustration">
                    <img src="multimedia/images/decoratives/ilustration-7.png" alt="">
                  </picture>
                  <figcaption class="description-container">
                    <h1 class="title">Institutions and Foundations</h1>
                    <p class="description">
                      USAT has developed extensive relationships with institutions and foundations spanning the US and China. We facilitate the alignment of interests across cultural barriers that otherwise require years of nuanced negotiation.
                    </p>
                  </figcaption>
                </figure>
                <figure class="image-container">
                  <picture class="ilustration">
                    <img src="multimedia/images/decoratives/ilustration-8.png" alt="">
                  </picture>
                  <figcaption class="description-container">
                    <h1 class="title">Medical Services &amp; Education</h1>
                    <p class="description">
                      Through our partnerships with the Chinese government and US academic institutions, we are developing extensive infrastructure for developing e-medicine technologies and services.
                    </p>
                  </figcaption>
                </figure>
              </div>
            </div>
          </section>
          <section id="consumer-goods">
            <div class="content-container">
              <div class="text-container">
                <header class="heading-container">
                  <hgroup class="heading-set">
                    <h4 class="sub-title">Trading</h4>
                    <h3 class="title">Consumer Goods</h3>
                  </hgroup>
                </header>
                <div class="text-set">
                  <p class="text">
                    Within China’s notoriously chaotic markets, USAT provides a network of trusted partners capable of expedited and secure importation, brand development within the Chinese media and social landscapes, and distribution within both cutting-edge mobile and brick-and-mortar facilities spanning the entire nation.
                  </p>
                  <p class="text">
                    USAT possesses exclusive rights for US partnerships seeking to leverage these platforms, and can therefore ensure that our family of brands maintains its standards of quality and integrity while scaling their presence within China. Furthermore, our partnerships with the Chinese government allow for expedited trademarking processes and the efficient management of conflicts with those who seek to infringe upon foreign brand identities before market entry.
                  </p>
                </div>
              </div>
              <div class="side-images">
                <figure class="image-container">
                  <picture class="ilustration">
                    <img src="multimedia/images/decoratives/ilustration-9.png" alt="">
                  </picture>
                  <figcaption class="description-container">
                    <h1 class="title">Cosmetic</h1>
                    <p class="description">
                      We provide the Chinese market with the highest quality US cosmetics, free from the pollutants and toxins commonly found in counterfeits, while protecting brand identity.
                    </p>
                  </figcaption>
                  <div class="button-container">
                    <button type="button" name="know-more"></button>
                  </div>
                </figure>
                <figure class="image-container">
                  <picture class="ilustration">
                    <img src="multimedia/images/decoratives/ilustration-10.png" alt="">
                  </picture>
                  <figcaption class="description-container">
                    <h1 class="title">Nutraceutical</h1>
                    <p class="description">
                      USAT is committed to facilitating the import of high quality nutraceutical products, incorporating insights from the frontiers of integrative medicine that appeal to both Eastern and Western sensibilities.
                    </p>
                  </figcaption>
                  <div class="button-container">
                    <button type="button" name="know-more"></button>
                  </div>
                </figure>
                <figure class="image-container">
                  <picture class="ilustration">
                    <img src="multimedia/images/decoratives/ilustration-11.png" alt="">
                  </picture>
                  <figcaption class="description-container">
                    <h1 class="title">Consumer &sol; Parented Technology</h1>
                    <p class="description">
                      USAT helps entrepreneurs, SMEs, and MNCs sell intellectual property protected products into China.
                    </p>
                  </figcaption>
                  <div class="button-container">
                    <button type="button" name="know-more"></button>
                  </div>
                </figure>
              </div>
            </div>
          </section>
        </section>
      </article>
      <section id="events-list">
        <div class="content-container">
          <header class="heading-container">
            <hgroup class="heading-set">
              <h3 class="sub-title">What's Happening</h3>
              <h2 class="title">Our Events</h2>
            </hgroup>
          </header>
          <div class="images-block">
            <figure class="image-container">
              <picture class="ilustration">
                <img src="multimedia/images/decoratives/city-3.jpg" alt="">
              </picture>
              <figcaption class="description-container">
                <h1 class="title">
                  Consectetur viverra, cursus cum beatae
                </h1>
                <p class="description">
                  Etiam vel malesuada sem. Fusce dapibus erat sit amet tortor tristique fringilla. Duis gravida dolor in sodales egestas. Donec convallis, ligula eu venenatis cursus, lectus sapien commodo enim, tempus malesuada justo ligula non tortor. Duis ac nisi felis. Nulla sem nisl, malesuada in pharetra vitae, porta quis velit. Donec blandit tellus vitae tellus lacinia hendrerit.
                </p>
              </figcaption>
              <div class="button-container">
                <button type="button" name="know-more"></button>
              </div>
            </figure>
            <figure class="image-container">
              <picture class="ilustration">
                <img src="multimedia/images/decoratives/health-2.jpg" alt="">
              </picture>
              <figcaption class="description-container">
                <h1 class="title">
                  Morbi aliquam purus in lectus egestas cursus
                </h1>
                <p class="description">
                  Fusce convallis lacus est, vitae rutrum sem consequat nec. Fusce tempus arcu tellus, eget venenatis mauris pulvinar in. Vivamus eu arcu tristique, rutrum tellus id, feugiat risus. Nulla eget sapien quam. Mauris at massa efficitur, dapibus ipsum eu, eleifend nibh.
                </p>
              </figcaption>
              <div class="button-container">
                <button type="button" name="know-more"></button>
              </div>
            </figure>
            <figure class="image-container">
              <picture class="ilustration">
                <img src="multimedia/images/decoratives/health-3.jpg" alt="">
              </picture>
              <figcaption class="description-container">
                <h1 class="title">
                  Nulla purus urna, interdum mattis ipsum sed
                </h1>
                <p class="description">
                  Phasellus vel risus sollicitudin, blandit mauris in, sagittis erat. Fusce ultrices eget nibh non faucibus. Pellentesque pretium turpis ac velit elementum aliquet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec congue sed ipsum at suscipit. Quisque volutpat velit nec mauris suscipit, eget accumsan nibh dictum.
                </p>
              </figcaption>
              <div class="button-container">
                <button type="button" name="know-more"></button>
              </div>
            </figure>
          </div>
        </div>
      </section>
      <section id="contact-us">
        <div class="content-container">
          <div class="side-content subscribe">
            <form action="" method="post" class="subscribe-form" autocomplete="off">
              <header class="heading-container">
                <hgroup class="heading-set">
                  <h2 class="sub-title">Subscribe</h2>
                  <h1 class="title">Follow us Now</h1>
                </hgroup>
              </header>
              <main class="form-body">
                <input type="email" name="email" placeholder="Your Email Address">
                <button type="submit" name="subscribe">Subscribe Now</button>
              </main>
            </form>
            <nav class="nav-container">
              <header class="heading-container">
                <h4 class="title">Our Social Media</h4>
              </header>
              <ul class="social-list">
                <li class="facebook"><a href="#"></a></li>
                <li class="twitter"><a href="#"></a></li>
                <li class="linkedin"><a href="#"></a></li>
                <li class="youtube"><a href="#"></a></li>
              </ul>
            </nav>
          </div>
          <div class="side-content contact">
            <form action="" method="post" class="contact-form" autocomplete="off">
              <header class="heading-container">
                <hgroup class="heading-set">
                  <h2 class="sub-title">Contact Us</h2>
                  <h1 class="title">
                    <span class="line">Are you a Premier</span>
                    <span class="line">USA Brand</span>
                  </h1>
                </hgroup>
                <div class="text-set">
                  <p class="text">
                    Complete section below for complete evaluation of your brand or product for the Chinese consumer market using the most advanced social mobile E-commerce platform and the most powerful and protective infrastructure for product sales in China.
                  </p>
                </div>
              </header>
              <main class="form-body">
                <div class="fields-container">
                  <div class="inputs-set">
                    <input type="text" name="name" placeholder="Your name">
                    <input type="email" name="email" placeholder="Your email address">
                  </div>
                  <textarea name="message" rows="5" cols="80" placeholder="Type your message..."></textarea>
                </div>
                <div class="button-container">
                  <button type="submit" name="contact-us">Send Now</button>
                </div>
              </main>
            </form>
          </div>
        </div>
      </section>
    </main>
    <footer id="top-footer">
      <section id="company-info">
        <div class="content-container">
          <div class="info-container">
            <picture class="side-image">
              <img src="multimedia/images/icons/logo-2.png" alt="logo">
            </picture>
            <div class="side-text">
              <div class="side-text">
                <section class="text-container address">
                  <header class="heading-container">
                    <h3 class="title">
                      <span class="line">US Authentic</span>
                      <span class="line">Trading Company</span>
                    </h3>
                  </header>
                  <address class="block-address">
                    <span class="line">4601 Wilshire Blvd, Beverly Hills</span>
                    <span class="line">California, USA 90212</span>
                  </address>
                </section>
                <section class="text-container contact">
                  <header class="heading-container">
                    <hgroup class="heading-set">
                      <h4 class="sub-title">Call Now</h4>
                      <h3 class="title">(310) 961 6276</h3>
                    </hgroup>
                  </header>
                  <div class="info-container">
                    <dl class="info-list">
                      <div class="list-set">
                        <dt class="term">Monday &ndash; Friday</dt>
                        <dd class="description">9am &ndash; 5pm</dd>
                      </div>
                      <div class="list-set">
                        <dt class="term">Saturday</dt>
                        <dd class="description">By appointment</dd>
                      </div>
                      <div class="list-set">
                        <dt class="term">Sunday</dt>
                        <dd class="description">Closed</dd>
                      </div>
                    </dl>
                  </div>
                </section>
              </div>
              <!-- <nav class="nav-container">
                <ul class="pages-list">
                  <li class="partnerships current"><a href="#">Partnerships &amp; Vision</a></li>
                  <li class="team"><a href="#">Team</a></li>
                  <li class="help"><a href="#">Get in Touch</a></li>
                </ul>
                <ul class="social-list">
                  <li class="facebook"><a href="#"></a></li>
                  <li class="twitter"><a href="#"></a></li>
                  <li class="linkedin"><a href="#"></a></li>
                  <li class="youtube"><a href="#"></a></li>
                </ul>
              </nav> -->
            </div>
          </div>
          <aside class="contact-action">
            <div class="content-container">
              <div class="text-container">
                <header class="heading-container">
                  <h3 class="title"><span class="removable">Better Yet, </span>Schedule a Meeting with our Team.</h3>
                </header>
                <div class="text-set">
                  <p class="text">
                    We have the best platform in China for you to reach your salesgoals, we would love to hear about your brand or product(s). There are limited spaces available per genre so please contact us as soon as possible.
                  </p>
                </div>
              </div>
              <div class="button-container">
                <button type="button" name="contact">Contact us Now</button>
              </div>
            </div>
          </aside>
        </div>
      </section>
      <div class="copyright">
        <div class="content-container">
          <p class="message"><span class="removable">Copyright </span>&copy; 2017 US Authentic Trading Company &ndash; All Rights Reserved.</p>
        </div>
      </div>
    </footer>
  </body>
</html>
